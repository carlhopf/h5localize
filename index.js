const MAP = {};

var DEFAULT_LANG = 'en';

exports.setDefaultLang = function(lang) {
  DEFAULT_LANG = lang;
};

exports.install = function(map, lang = 'en') {
  if (!MAP[lang]) MAP[lang] = {};

  Object.keys(map).forEach(key => {
    MAP[lang][key] = map[key].value;
  });
};

exports.get = function(key, lang = DEFAULT_LANG) {
  if (!MAP[lang]) MAP[lang] = {};
  return MAP[lang][key] || key + '-miss';
};
